//
//  SecondViewController.swift
//  expenseTracker
//
//  Created by Canyon Duncan on 10/4/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController {

    @IBOutlet weak var nameOfPlace: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var formOfPayment: UITextField!
    @IBOutlet weak var datePaid: UITextField!
    @IBAction func datePicker(_ sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(getter: SecondViewController.datePaid), for: UIControlEvents.valueChanged)
        
    }
    
     let firstVC: ViewController = ViewController(nibName: nil, bundle: nil)
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blue
        // Do any additional setup after loading the view.
        let savebutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttontapped))
        self.navigationItem.rightBarButtonItem = savebutton
    
        
    
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttontapped(_sender:UIBarButtonItem){
       // let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
       // let paied = Payment(context: context)
        let name = nameOfPlace.text
        let amountPaid = amount.text
        let catagory = category.text
        let paymentMethod = formOfPayment.text
        let date = datePaid.text
        
        self.save(nameOfPlace: name!, amount: amountPaid!, catagory: catagory!, paymentMethod: paymentMethod!, date: date!)
        
        
    }
    
    
    
    func save(nameOfPlace: String, amount: String, catagory: String, paymentMethod: String, date: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Payment", in: managedContext)!
        let payment = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        payment.setValue(nameOfPlace, forKeyPath: "nameOfPlace")
        payment.setValue(amount, forKey: "amount")
        payment.setValue(catagory, forKey: "catagory")
        payment.setValue(paymentMethod, forKey: "paymentMethod")
        payment.setValue(date, forKey: "date")
        
        // 4
        do {
            try managedContext.save()
            print("i made it to the save")
            //firstVC.expense.append(payment)
            //firstVC.tableView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        //firstVC.expense.append(payment)
       // firstVC.tableView.reloadData()
        
        _ = navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
